import React, { Component } from 'react'
import Auxx from '../../../hoc/Auxx/Auxx'
import Button from '../../UI/Button/Button'

class OrderSummary extends Component {

    render() {

        const ingredients = Object.keys(this.props.ingredients).
            map((key) => {
                return <li key={key}><span style={{ textTransform: 'capitalize' }}>{key} </span> : {this.props.ingredients[key]}</li>
            })

        return (
            <Auxx>
                <h3>Your Order</h3>
                <p><strong>Total Price : {this.props.price}</strong></p>
                <p>A Delicious burger with following ingredients:</p>
                <ul>
                    {ingredients}
                </ul>
                <p>Continue to checkout ?</p>
                <Button clicked={this.props.purchaseCancel} btnType="Danger">Cancel</Button>
                <Button clicked={this.props.purchaseContinue} btnType="Success">Continue</Button>
            </Auxx>
        )
    }


}

export default OrderSummary