import React from 'react'
import classes from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl'

const BuildControls = (props) => {

    var controls = [
        { label: 'Salad', type: 'salad' },
        { label: 'Cheese', type: 'cheese' },
        { label: 'Meat', type: 'meat' },
        { label: 'Bacon', type: 'bacon' },
    ]

    return (
        <div className={classes.BuildControls}>
            <p>Current Price: <b>{props.price}</b></p>
            {
                controls.map(ctrl => { 
                    return (
                        <BuildControl 
                        key={ctrl.label} 
                        label={ctrl.label} 
                        type={ctrl.type}
                        added={()=>props.ingredientAdded(ctrl.type)} 
                        removed={()=>props.ingredientRemoved(ctrl.type)}
                        disabled = {props.disabled[ctrl.type]}></BuildControl>
                    ) })
            }
            <button onClick = {props.ordered} className= {classes.Order} disabled={props.purchasable}>ORDER NOW</button>

        </div>
    )
}

export default BuildControls