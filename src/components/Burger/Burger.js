import React from 'react'
import classes from './Burger.module.css'
import BurgerIngredients from './BurgerIngrendient/BurgerIngredient'

const Burger = (props) => {

    let key = Object.keys(props.ingredients).map(keys => {
        return [...Array(props.ingredients[keys])]
            .map((_, i) => { return <BurgerIngredients key={keys + i} type={keys} /> })
    }).reduce((arr, el) => { return arr.concat(el) }, [])

    if (key == 0) {
        key = <p>Please input ingredients</p>
    }

    return (
        <div className={classes.Burger}>
            <BurgerIngredients type='bread-top' />
            {key}
            <BurgerIngredients type='bread-bottom' />

        </div>
    )
}

export default Burger;