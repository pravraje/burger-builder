import React, { Component } from "react";
import Auxx from '../../hoc/Auxx/Auxx'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'


let INGRIDIENT_PRICE = {
    salad: 2,
    bacon: 3,
    cheese: 4,
    meat: 5
}

class BurgerBuilder extends Component {

    state = {
        ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        },
        total: 4,
        purchasing: false,
        loading : false
    }

    addIngredientHandler = (type) => {

        let oldCount = this.state.ingredients[type];
        let updatedCount = oldCount + 1;
        let updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;

        const oldTotal = this.state.total;
        const updatedTotal = oldTotal + INGRIDIENT_PRICE[type];

        this.setState({
            ingredients: updatedIngredients, total: updatedTotal
        })

    }

    removeIngredientHandler = (type) => {

        let oldCount = this.state.ingredients[type];
        let updatedCount = oldCount - 1;
        let updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;

        const oldTotal = this.state.total;
        const updatedTotal = oldTotal - INGRIDIENT_PRICE[type];

        if (updatedCount < 0) {
            return;
        }

        this.setState({
            ingredients: updatedIngredients, total: updatedTotal
        })

    }

    purchasingHandler = () => {
        this.setState({ purchasing: true })
    }

    purchaseCancelHandler = () => {
        this.setState({ purchasing: false })
    }

    purchaseContinue = () => {
        this.setState({loading:true});

        console.log(this.state);
        const orders = {
            ingredients : this.state.ingredients,
            total : this.state.total,
            customer : {
                name : 'Pravinkumar',
                address : 'Coimbatore, India'
            },
            delivery_type : 'fast'
        }
        axios.post('/order',orders).
        then(response => {
            this.setState({loading:false});
            this.setState({purchasing:false});
            console.log(response.data)})
        .catch(error => {
            this.setState({loading:false});
            console.log(error)})
    }



    

    render() {
        const disabledInfo = { ...this.state.ingredients };
        let orderSummary;

        for (const key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
        }

        if(this.state.loading){
            orderSummary = <Spinner/>
        }
        else {
            orderSummary = <OrderSummary ingredients={this.state.ingredients} purchaseContinue = {this.purchaseContinue} price={this.state.total}></OrderSummary>
        }
        
        return (
            <Auxx>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                    ingredientAdded={this.addIngredientHandler}
                    ingredientRemoved={this.removeIngredientHandler} disabled={disabledInfo}
                    price={this.state.total}
                    purchasable={this.state.total == 4}
                    ordered={this.purchasingHandler}>
                </BuildControls>
            </Auxx>
        );
    }
}

export default withErrorHandler(BurgerBuilder,axios);