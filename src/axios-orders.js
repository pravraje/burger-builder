// https://tasty-burger-builder.firebaseio.com/

import axios from 'axios'

const instance = axios.create({
    baseURL : 'https://tasty-burger-builder.firebaseio.com/'
});

export default instance;
